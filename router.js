const Authentication = require('./controllers/authentication');
const passportService = require('./services/passport');
const passport = require('passport');
const API_ROOT_URL = '/api/';
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });
const User = require('./controllers/users');
const Comment = require('./controllers/comments');
const Battle = require('./controllers/battle');

module.exports = function(app){
    app.get('/', requireAuth, function(req, res){
        res.send({ message:"Super secret code is ABC123"});
    });
    app.post('/signin',requireSignin, Authentication.signin);
    app.post('/signup', Authentication.signup);


    app.get(`${API_ROOT_URL}users`, User.fetchUsers);
    app.delete(`${API_ROOT_URL}user/:id`,requireAuth, User.deleteUser);
    app.post(`${API_ROOT_URL}user/:id/comment/new`, Comment.createComment);

    app.post(`${API_ROOT_URL}battle/new`,requireAuth, Battle.createBattle);
    app.get(`${API_ROOT_URL}battle/:id`,requireAuth, Battle.getBattle);
    app.get(`${API_ROOT_URL}battles`, Battle.getBattles);
    app.delete(`${API_ROOT_URL}battle/:id`, Battle.deleteBattle);

}