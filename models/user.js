'use strict';
const bcrypt = require('bcrypt-nodejs');
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        User.hasMany(models.Comment, { foreignKey:'user_id'});
        User.hasMany(models.Battle, { foreignKey:'user_id'});
      }
    },
    instanceMethods: {
        comparePassword : function(candidatePassword, callback){
                bcrypt.compare(candidatePassword, this.password, function(err, isMatch){
                if(err){ return callback(err); }

                callback(null, isMatch); 
            })
        }
    }
  });

  User.hook('afterCreate', function(user, options) {
    bcrypt.genSalt(10, function(err, salt){
        if(err) { return next(err); }

        bcrypt.hash(user.password, salt, null, function(err, hash){
            if(err) { return next(err); }
            return User.update({password: hash}, {where: {id: user.id}});
        });
    });
  });

  User.retrieve = function(id, success, err) {
    User.find({where: { id:id}})
        .then(function(user){
            success(user);
        },function(err){
            err(err);
        })
  };

  return User;
};