'use strict';
module.exports = function(sequelize, DataTypes) {
  var Battle = sequelize.define('Battle', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    preparation_time: DataTypes.DATE,
    user_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        Battle.belongsTo(models.User, {foreignKey:'user_id'})
        Battle.hasOne(models.BattleSong, {foreignKey:'battle_id'})
      }
    }
  });
  return Battle;
};