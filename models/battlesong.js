'use strict';
module.exports = function(sequelize, DataTypes) {
  var BattleSong = sequelize.define('BattleSong', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    url: DataTypes.TEXT,
    embedded_url: DataTypes.TEXT,
    battle_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        BattleSong.belongsTo(models.Battle, {foreignKey:'battle_id'})
      }
    }
  });
  return BattleSong;
};