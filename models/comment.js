'use strict';
module.exports = function(sequelize, DataTypes) {
  var Comment = sequelize.define('Comment', {
    parent_id: DataTypes.INTEGER,
    text: DataTypes.TEXT,
    user_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        Comment.belongsTo(models.User, { foreignKey:'user_id'});
      }
    }
  });
  return Comment;
};