const passport = require('passport');
const User = require('../models').User
const config = require('../db/config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

//LOCAL STRATEGY

const localOptions = { usernameField: 'email'};
const localLogin = new LocalStrategy(localOptions, function(email, password, done){
    User.findOne({where:{ email:email}})
        .then(function(user){
            if(!user) {return done(null, false);}
            user.comparePassword(password, function(err, isMatch){
                if(err) { return done(err); }
                if(!isMatch) { return done(null, false); }

                return done(null, user);
            })

        }, function(err){
            return done(err); 
        })

});


// JWT STRATEGY
const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config().secret
};

//testetes

const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done){

    User.findOne({where:{ id:payload.sub}})
        .then(function(user){
            if(user){
                done(null, user);
            } else {
                done(null, false);
            }
        }, function(err){
            return done(err, false);
        })
});

passport.use(jwtLogin);
passport.use(localLogin);