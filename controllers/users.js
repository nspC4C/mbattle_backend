const User = require('../models').User;
const _ = require('lodash');

exports.fetchUsers = function(req, res, next){
    User.findAll({attributes: ['username','email', 'createdAt', 'updatedAt', 'id']})
        .then(function(users){
                res.json(users);
        },function(err){
            return next(err); 
        })

}

exports.deleteUser = function(req, res, next){
    User.destroy({
        where: {
            id:req.params.id
        }
    }).then(function(){
        res.send("User succesfully deleted");
    })
}