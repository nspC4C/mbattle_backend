const jwt = require('jwt-simple');
const User = require('../models').User;
const config = require('../db/config');

function tokenForUser(user){
    const timestamp = new Date().getTime();
    return jwt.encode({ sub: user.id, iat: timestamp }, config().secret)
}

exports.signin = function(req, res, next){
    res.send( {token:tokenForUser(req.user)} );
}

exports.signup = function(req, res, next){
    console.log(req.body);
    const email = req.body.email;
    const password = req.body.password;
    const username = req.body.username;

    if(!email || !password){
        return res.status(422).send({ error: 'You must provide email and password' });
    }

    User.findOne({ where: {email: email }})
        .then(function(existingUser){
            if(existingUser){
                return res.status(422).send({ error: 'Email is in use' });
            }
            console.log(User , "authnetication.js linia 28");
            User.build({email: email, password: password, username:username})
                .save()
                .then(function(user) {
                    res.json({ token: tokenForUser(user) });
                }).catch(function(err) {
                    if(err){ return next(err) }; 
                })
            
        }, function(err){
            if(err) { return next(err) ;}
        });
}