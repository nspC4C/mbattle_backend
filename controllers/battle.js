const ConvertToEmbeddedUrl = require('../helpers').ConvertToEmbeddedUrl;
const GetIdfromUrl = require('../helpers').GetId;

const BattleSong = require('../models').BattleSong;
const Battle = require('../models').Battle;
const User = require('../models').User

var YouTube = require('youtube-node');
var youTube = new YouTube();
youTube.setKey('AIzaSyDbe-g5F-oq2L-zOGHGJIi7N9bdfujlOCM');


exports.createBattle = function(req, res, next){
    
    if(req.user){
        const User = req.user;
        const New_Battle = {
            name:req.body.name,
            description:req.body.description,
            preparation_time:req.body.preparation_time,
        } 
        // CREATE BATTLE
        User.createBattle(New_Battle)
            .then(function(battle){
                //CREATE BATTLE SONG
                const YoutubeURL = req.body.song_url;
                const battle_song_embedded_url = ConvertToEmbeddedUrl(YoutubeURL);
                const youtubeID = GetIdfromUrl(YoutubeURL);
                //RETRIEVE SONG FROM YOUTUBE BY YT ID
                youTube.getById(youtubeID, function(err, result){
                    if(err){ 
                        return next(err) 
                    } else {
                        const YT_song = result.items[0].snippet;
                        const New_Battle_Song = {
                            name:YT_song.title,
                            description:YT_song.description,
                            url:YoutubeURL,
                            embedded_url:battle_song_embedded_url,
                            battle_id:battle.id
                        }
                        //SAVE BATTLE SONG
                        BattleSong.build(New_Battle_Song).save()
                            .then(function(song) {
                                res.json({ battle: battle });
                            }).catch(function(err) {
                                if(err){ return next(err) }; 
                            })                               
                    }
                })
                
            },
            function(err){
                if(err){ return next(err) }; 
            })
    } else {
        res.json({ error: "You can't create battle" });
    }

};

exports.getBattle = function(req, res, next){
    Battle.findOne({
        where: {id:req.params.id},
        include:[BattleSong, { model: User, attributes:['email']}]
    })
        .then(function(battle){
            if(battle){
                res.json({battle:battle})
            } else {
                res.json({})
            }
        }, 
         function(err){
             return next(err)
        })
    
}

exports.getBattles = function(req, res, next){
    Battle.findAll({include:[BattleSong, { model: User, attributes:['email', 'username']}]})
        .then(function(battles){
            if(battles){
                res.json({battles:battles})
            }
        }, 
         function(err){
             return next(err)
        })
}

exports.deleteBattle = function(req, res, next){
    Battle.findOne({where:{id:req.params.id}})
        .then(function(battle){
            battle.destroy()
                .then(function(success){
                    res.send("Battle deleted successfully");
                })
        },
        function(err){
            return err;
        })
}