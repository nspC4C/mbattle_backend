const Comment = require('../models').Comment;
const User = require('../models').User;


exports.createComment = function(req, res, next){
    User.retrieve(req.params.id, 
        function(user){
            if(user){
                user.createComment({text:req.body.text})
                    .then(function(comment) {
                            res.json({ comment: comment });
                    }).catch(function(err) {
                            if(err){ return next(err) }; 
                    })
            } else {
                res.json({ error: "You can't add comment" });
            }
            
        }, 
        function(err){
            res.json({ error: "Something was wrong...." });
        })
}